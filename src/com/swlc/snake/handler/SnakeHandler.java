package com.swlc.snake.handler;

import com.swlc.snake.main.GameInitializer;
import com.swlc.snake.view.Food;
import com.swlc.snake.view.MainPanel;
import com.swlc.snake.view.Snake;

/**
 * This class is responsible for running the game, or indeed making the
 * snake move.
 *
 */
public class SnakeHandler implements Runnable {

	// The amount of time in miliseconds between each 'tick'
	public static final int DELAY = 250;

	private GameInitializer frame;
	private MainPanel mainPanel;
	private Snake snake;
	private Food food;

	/**
	 * Constructs a new runnable Game object which can be used to create
	 * a Thread.
	 * @param mainPanel The rectangular area where snake can move
	 * @param snake The snake object
	 * @param frame The frame which will be notified when the game is over
	 */
	public SnakeHandler(MainPanel mainPanel, Snake snake, GameInitializer frame) {
		food = new Food(getNew(), getNew());
		this.frame = frame;
		this.snake = snake;
		this.mainPanel = mainPanel;

		this.mainPanel.setSnakeParts(snake.getParts());
		this.mainPanel.setFood(food);
	}

	private double getNew() {
		double d = 1111;

		while (d >= 500 || d % 20 != 0) {
			d = Math.random() * 1000;
			d = (int) d;
		}
		return d;
	}

	@Override
	public void run() {
		try {
			while (true) {
				snake.move();
				snake.check();
				if (snake.isGameOver()) {
					Thread.currentThread().interrupt();
				}
				if (!Thread.currentThread().isInterrupted()) {
					mainPanel.repaint();
				}
				Thread.sleep(DELAY);
			}
		} catch (InterruptedException ex) {
			frame.gameOver();
		}
	}
}
