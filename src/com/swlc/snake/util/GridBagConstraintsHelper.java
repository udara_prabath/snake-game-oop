package com.swlc.snake.util;

import java.awt.*;

/**
* This class simplifies the use of the GridBagConstraints class.
*/
public class GridBagConstraintsHelper extends GridBagConstraints {

	public GridBagConstraintsHelper(int gridx, int gridy) {
		this.gridx = gridx;
		this.gridy = gridy;
	}

	public GridBagConstraintsHelper(int gridx, int gridy, int gridwidth, int gridheight) {
		this(gridx, gridy);
		this.gridwidth = gridwidth;
		this.gridheight = gridheight;
	}
}
