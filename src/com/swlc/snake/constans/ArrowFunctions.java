package com.swlc.snake.constans;

/**
 * Enumeration data type which describes one of the 4 possible directions
 * snake can move.
 */
public enum ArrowFunctions {
	UP, DOWN, LEFT, RIGHT
}

class ScorePanel{
	public static final int PANEL_WIDTH = 500;
	public static final int PANEL_HEIGHT = 50;
}

class GamePanel {
	public static final int PANEL_WIDTH = 500;
	public static final int PANEL_HEIGHT = 500;
}
class Snake {
	public static final int XSIZE = 0;
	public static final int YSIZE = 20;
}
