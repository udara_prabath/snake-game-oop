package com.swlc.snake.view;

import java.awt.geom.Ellipse2D;

/**
 * This class represents an food which can be ate by the snake. It appears
 * randomly somewhere within the game field.
 *
 */
public class Food {

	public static final int XSIZE = 20;
	public static final int YSIZE = 20;

	private double x;
	private double y;

	/**
	 * Creates an food with given coordinates
	 * @param x The x coordinate of food's upper left corner
	 * @param y The y coordinate of food's upper left corner
	 */
	public Food(double x, double y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Generates new coordinates for the food, so it can appear
	 * somewhere else
	 * @param snake The snake
	 */
	public void next(Snake snake) {
		for (Ellipse2D.Double e : snake.getParts()) {
			while (x == e.getMinX() && y == e.getMinY()) {
				x = getNew();
				y = getNew();
			}
		}
	}

	/*
	 * Generates a random number which can be used as a valid coordinate
	 */
	private double getNew() {
		double d = 1111;

		while (d >= 500 || d % 20 != 0) {
			d = Math.random() * 1000;
			d = (int) d;
		}
		return d;
	}

	/**
	 * @return The shape of the food
	 */
	public Ellipse2D.Double getShape() {
		return new Ellipse2D.Double(x, y, XSIZE, YSIZE);
	}
}
