package com.swlc.snake.view;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * GameField represents a black, rectangular area where the snake can move.
 * It is also responsible for drawing the snake and the food.
 *
 */
public class MainPanel extends JPanel {

	public static final int PANEL_WIDTH = 500;
	public static final int PANEL_HEIGHT = 500;

	private List<Ellipse2D.Double> snakeParts;
	private Food food;
	/**
	 * Constructs the game field, which is the rectangular area where snake can
	 * move
	 */
	public MainPanel() {
		setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
		setBackground(Color.darkGray);
		initDefaults();
	}

	/**
	 * Initializes the default snake and the apple
	 */
	public void initDefaults() {
		food = new Food(100, 100);
		snakeParts = Collections
				.synchronizedList(new ArrayList<Ellipse2D.Double>());
		snakeParts.add(new Ellipse2D.Double(260, 260, 20, 20));
		snakeParts.add(new Ellipse2D.Double(260, 280, 20, 20));
	}

	public void setSnakeParts(List<Ellipse2D.Double> snakeParts) {
		this.snakeParts = snakeParts;
	}

	public void setFood(Food food) {
		this.food = food;
	}

	public Food getFood() {
		return food;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;

		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		// Draw the apple
		g2.setPaint(Color.WHITE);
		g2.fillOval((int) food.getShape().getMinX() + 5, (int) food.getShape()
				.getMinY() + 5, 10, 10);

		// Draw the snake parts
		g2.setPaint(new Color(217, 155, 165));
		for (Ellipse2D e : snakeParts) {
			Rectangle rect = new Rectangle((int)e.getMinX(), (int)e.getMinY(), 20, 20);
			g2.fill(rect);
		}
		// Draw the head of the snake
		g2.setPaint(new Color(255, 1, 28));
		Rectangle rect = new Rectangle((int)snakeParts.get(0).getMinX(), (int)snakeParts.get(0).getMinY(), 20, 20);
		g2.fill(rect);
	}
}
