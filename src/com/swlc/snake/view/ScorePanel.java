package com.swlc.snake.view;

import javax.swing.*;
import java.awt.*;

/**
 * This panel is responsible for displaying the current score of thr user.
 *
 */
public class ScorePanel extends JPanel {

	public static final int PANEL_WIDTH = 500;
	public static final int PANEL_HEIGHT = 50;

	private final Font FONT;
	private final String SCORE_LABEL = "SCORE: ";
	private String score;

	public ScorePanel() {
		setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
		setBackground(Color.BLACK);

		score = "0";
		FONT = new Font("SansSerif", Font.BOLD, 20);
	}

	/**
	 * Update current score.
	 * @param points The amount of points
	 */
	public void addPoints(int points) {
		int oldValue = Integer.parseInt(score);
		oldValue += points;
		score = oldValue + "";
		repaint();
	}

	public String getScore(){
		return score;
	}

	/**
	 * Clears the score back to its intial value of 0
	 */
	public void clear() {
		score = "0";
	}


	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;

		g2.setFont(FONT);
		g2.setPaint(new Color(255, 255, 255));
		g2.drawString(SCORE_LABEL, 15, 32);
		g2.setPaint(new Color(226, 179, 6));
		g2.drawString(score, 105, 32);
	}
}
