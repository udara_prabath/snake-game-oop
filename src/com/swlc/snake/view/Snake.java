package com.swlc.snake.view;

import com.swlc.snake.constans.ArrowFunctions;

import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class represents a snake which is represented as a list of snake parts
 * and each part is a 20 x 20 px ellipse.
 *
 */
public class Snake {

    // size of a single snake part
    public static final int XSIZE = 0;
    public static final int YSIZE = 20;
    public String state = "LRRR";
    private MainPanel mainPanel;
    private ScorePanel scorePanel;
    private List<Ellipse2D.Double> snakeParts;
    private ArrowFunctions arrowFunctions;

    private Ellipse2D.Double temp;
    private Ellipse2D.Double ass;

    private boolean over = false;

    public Snake(){
        initDefaults();
    }


    /**
     * Constructs a snake object with the default snake parts
     * @param mainPanel The field on which snake can move
     * @param scorePanel The panel for displaying user score
     */
    public Snake(MainPanel mainPanel, ScorePanel scorePanel) {
        this.mainPanel = mainPanel;
        this.scorePanel = scorePanel;
        initDefaults();
    }

    /**
     * Changes snake's direction
     * @param arrowFunctions The new direction
     */
    public void changeDirection(ArrowFunctions arrowFunctions) {
        this.arrowFunctions = arrowFunctions;
    }

    /**
     * Moves the snake in the current direction
     */
    public void move() {
        switch (arrowFunctions) {
            case UP:
                moveBody();
                // head
                snakeParts.set(0, new Ellipse2D.Double(snakeParts.get(0).getMinX(),
                        snakeParts.get(0).getMinY() - 20, XSIZE, YSIZE));
                if (snakeParts.get(0).getMinY() < 0) {
                    over = true;
                }
                break;

            case DOWN:
                moveBody();
                // head
                snakeParts.set(0, new Ellipse2D.Double(snakeParts.get(0).getMinX(),
                        snakeParts.get(0).getMinY() + 20, XSIZE, YSIZE));
                if (snakeParts.get(0).getMaxY() > mainPanel.getBounds().getMaxY()) {
                    over = true;
                }
                break;

            case LEFT:
                moveBody();
                // head
                snakeParts.set(0, new Ellipse2D.Double(
                        snakeParts.get(0).getMinX() - 20, snakeParts.get(0)
                        .getMinY(), XSIZE, YSIZE));
                if (snakeParts.get(0).getMinX() < 0) {
                    over = true;
                }
                break;

            case RIGHT:
                moveBody();
                // head
                snakeParts.set(0, new Ellipse2D.Double(
                        snakeParts.get(0).getMinX() + 20, snakeParts.get(0)
                        .getMinY(), XSIZE, YSIZE));
                if (snakeParts.get(0).getMaxX() > mainPanel.getBounds().getMaxX()) {
                    over = true;
                }
                break;

            default:
                new Exception("Unexcepted Direction value!").printStackTrace();
                break;
        }
    }

    /**
     * @return snakeParts The list containing snake parts
     */
    public List<Ellipse2D.Double> getParts() {
        return snakeParts;
    }

    /**
     * Checks if the snake ate the food or ate itself
     */
    public void check() {
        Ellipse2D.Double head = snakeParts.get(0);
        Food food = mainPanel.getFood();

        // Ate itself
        for (int i = 1; i < snakeParts.size(); i++) {
            if (head.getMinX() == snakeParts.get(i).getMinX()
                    && head.getMinY() == snakeParts.get(i).getMinY()) {
                over = true;
                return;
            }
        }
        // Ate food
        if (head.getMinX() == food.getShape().getMinX()
                && head.getMinY() == food.getShape().getMinY()) {
            scorePanel.addPoints(1);
            food.next(this);
            snakeParts.add(ass);
        }
    }

    /**
     * @return true if game over, false otherwise
     */
    public boolean isGameOver() {
        return over;
    }

    private void moveBody() {
        for (int i = snakeParts.size() - 1; i > 0; i--) {
            if (i == snakeParts.size() - 1) {
                ass = (Ellipse2D.Double) snakeParts.get(i).clone();
            }
            temp = (Ellipse2D.Double) snakeParts.get(i - 1).clone();
            snakeParts.set(i, temp);
        }
    }

    private void initDefaults() {
        snakeParts = Collections
                .synchronizedList(new ArrayList<Ellipse2D.Double>());
        snakeParts.add(new Ellipse2D.Double(260, 260, 20, 20));
        snakeParts.add(new Ellipse2D.Double(260, 280, 20, 20));
    }

    public int getSnakeSize(){
        return this.snakeParts.size();
    }
    public String getCurrentDirection() {
        return "" + this.state.charAt(0);
    }
    public void headUp() {
        if (!"D".equals(this.getCurrentDirection()))
            this.state = "U" + this.state.substring(1);;
    }
    public void headDown() {
        if (!"U".equals(this.getCurrentDirection()))
            this.state = "D" + this.state.substring(1);
    }

    public void headLeft() {
        if (!"R".equals(this.getCurrentDirection()))
            this.state = "L"  + this.state.substring(1);
    }

    public void headRight() {
        if (!"L".equals(this.getCurrentDirection()))
            this.state = "R" + this.state.substring(1);
    }
}
