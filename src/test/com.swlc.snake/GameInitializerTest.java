package test.com.swlc.snake;

import com.swlc.snake.constans.ArrowFunctions;
import com.swlc.snake.handler.SnakeHandler;
import com.swlc.snake.util.GridBagConstraintsHelper;
import com.swlc.snake.view.MainPanel;
import com.swlc.snake.view.ScorePanel;
import com.swlc.snake.view.Snake;
import org.junit.Test;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class GameInitializerTest extends JFrame {

    private ArrowFunctions arrowFunctions = ArrowFunctions.UP;
    private ScorePanel scorePanel;
    private MainPanel mainPanel;
    private Thread thread;
    private Snake snake;
    private boolean started = false;

    @Test
    public void gameInitializer() {
        initComponents();
        initGame();
        initFrame();
    }

    @Test
    public void initComponents() {
        setLayout(new GridBagLayout());
        addKeyListener(new KeyboardHandler());

        mainPanel = new MainPanel();
        add(mainPanel, new GridBagConstraintsHelper(0, 0, 8, 8));

        scorePanel = new ScorePanel();
        add(scorePanel, new GridBagConstraintsHelper(0, 8, 8, 1));

    }

    @Test
    public void initGame() {
        snake = new Snake(mainPanel, scorePanel);
        Runnable r = new SnakeHandler(mainPanel, snake, null);
        thread = new Thread(r);
    }

    @Test
    public void initFrame() {
        pack();
        setTitle("Snake");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);
    }

    @Test
    public void newGame() {
        started = true;
        thread.start();
    }

    @Test
    public void gameOver() {
        int returnValue = JOptionPane.showConfirmDialog(this,
                "Your score is " + scorePanel.getScore() + "\n Do you want to start a new game?", "GAME OVER!", JOptionPane
                        .OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);

        if (returnValue == JOptionPane.OK_OPTION) {
            arrowFunctions = ArrowFunctions.UP;
            started = false;
            snake = new Snake(mainPanel, scorePanel);
            scorePanel.clear();
            mainPanel.initDefaults();
            scorePanel.repaint();
            mainPanel.repaint();
            Runnable r = new SnakeHandler(mainPanel, snake, null);
            thread = null;
            thread = new Thread(r);
        } else {
            System.exit(0);
        }
    }

    private class KeyboardHandler extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                if (arrowFunctions == ArrowFunctions.DOWN) return;
                if (!started) newGame();
                if (snake != null) {
                    snake.changeDirection(ArrowFunctions.UP);
                    arrowFunctions = ArrowFunctions.UP;
                }
            }

            if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                if (arrowFunctions == ArrowFunctions.UP) return;
                if (!started) newGame();
                if (snake != null) {
                    snake.changeDirection(ArrowFunctions.DOWN);
                    arrowFunctions = ArrowFunctions.DOWN;
                }
            }

            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                if (arrowFunctions == ArrowFunctions.RIGHT) return;
                if (!started) newGame();
                if (snake != null) {
                    snake.changeDirection(ArrowFunctions.LEFT);
                    arrowFunctions = ArrowFunctions.LEFT;
                }
            }

            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                if (arrowFunctions == ArrowFunctions.LEFT) return;
                if (!started) newGame();
                if (snake != null) {
                    snake.changeDirection(ArrowFunctions.RIGHT);
                    arrowFunctions = ArrowFunctions.RIGHT;
                }
            }
        }
    }
}
