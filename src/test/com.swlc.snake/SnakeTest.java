package test.com.swlc.snake;

import com.swlc.snake.view.Snake;
import org.junit.Test;
import static org.junit.Assert.*;

public class SnakeTest {

    @Test
    public void aSnakeIs3UnitsLongByDefault() {
        Snake snake = new Snake();
        int length = snake.getSnakeSize();
        assertEquals(3, length);
    }

    @Test
    public void canHeadUp() {
        Snake snake = new Snake();
        snake.headUp();
        String direction = snake.getCurrentDirection();
        assertEquals("U", direction);
    }

    @Test
    public void canHeadDown() {
        Snake snake = new Snake();
        snake.headDown();
        String direction = snake.getCurrentDirection();
        assertEquals("D", direction);
    }

    @Test
    public void canHeadRight() {
        Snake snake = new Snake();
        snake.state = "ULLL";
        snake.headRight();
        String direction = snake.getCurrentDirection();
        assertEquals("R", direction);
    }
    @Test
    public void isFullyExtendedAtTheBeginning() {
        Snake snake = new Snake();
        assertEquals("LRRR", snake.state);
    }

    @Test
    public void cannotHeadBackwards() {
        Snake snake = new Snake();
        snake.state = "RLLL";
        snake.headLeft();
        assertEquals("RLLL", snake.state);
        snake.state = "LRRR";
        snake.headRight();
        assertEquals("LRRR", snake.state);
        snake.state = "UDDD";
        snake.headDown();
        assertEquals("UDDD", snake.state);
        snake.state = "DUUU";
        snake.headDown();
        assertEquals("DUUU", snake.state);
    }

}
